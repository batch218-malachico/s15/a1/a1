console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	// let fullName = "Steve";
	// console.log("fullName" +);

	let firstName = "First Name: John";
	console.log(firstName);

	let lastName = "Last Name: Smith";
	console.log(lastName)

	let age = "Age: 30";
	console.log(age);

	let hobbies = "Hobbies:"
	console.log(hobbies)

	const hobby = ['Biking', 'Mountain Climbing', 'Swimming'];
	console.log(hobby)

	let workAddress = "Work Address:"
	console.log(workAddress)

	let address = {

	houseNumber: '32',
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska',
	};
	console.log(address)


	let myfullName = "My full name is:";
	let name = "Steve Rogers";
	console.log(myfullName + ', ' + name);


	let mycurrentAge = "My current age is: 40";
	console.log(mycurrentAge);

	let myfriendsAre = "My friends are:";
	console.log(myfriendsAre);

	let friendsAre= ['Tony', 'Bruce', 'Thor', 'Natasha', 'Clint', 'Nik'];
	console.log(friendsAre);

	let profile = "My Full Profile";
	console.log(profile);

	let person = {

	username: 'captain_america',
	fullName: 'Steve Rogers',
	age: 40,
	isActive: false,
	};
	console.log(person)

	let bestfriends = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriends);

	const mylastLocation = "Arctic Ocean";
	let Location = "I was found frozen in:";
	console.log("I was found frozen in: " + mylastLocation);

